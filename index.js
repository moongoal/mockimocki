/**
  Mock data switch module

  @license BSD-3-Clause
*/

let supplyMockData = false; // Global supply setting

const SUPPLY_REAL = 'real';
const SUPPLY_MOCK = 'mock';
const SUPPLY_ALLOWED = [
  SUPPLY_REAL,
  SUPPLY_MOCK
];
const SUPPLY_DEFAULT = SUPPLY_REAL;

/**
  Set global supply mode.

  @param {String} what ['real'] - Either 'real' or 'mock'
*/
function supply(
  what = SUPPLY_DEFAULT // This default is meant for debugging purpose **only**, don't rely on it.
) {
  if (SUPPLY_ALLOWED.includes(what)) {
    supplyMockData = (what === SUPPLY_MOCK);
  } else {
    throw new Error(
      `Invalid "what" option provided. Value is "${what}", but can only be one of ${SUPPLY_ALLOWED.join(', ')}`
    );
  }
}

/**
  Return one of `real()` or `mock()` based on global supply mode.

  @param {Function} real - Function returning the real data
  @param {Function} mock - Function returning the mock data
  @param {Object=} opts - Options

  Options
  @param {String} force - Locally override global supply mode ({@link supply})

  @return The real data as returned by the `real` function
    or the mock data as returned by the `mock` function
*/
function mockimocki(real, mock, { force } = {}) {
  let f = supplyMockData ? mock : real;

  if (force) {
    if (SUPPLY_ALLOWED.includes(force)) {
      console.warn(`Supplying ${force} data`);
      console.trace();

      f = (force === SUPPLY_REAL) ? real : mock;
    } else {
      throw new Error(
        `Invalid "force" option provided. Value is "${force}", but can only be one of ${SUPPLY_ALLOWED.join(', ')}`
      );
    }
  }

  return f();
}

mockimocki.mockimocki = mockimocki;
mockimocki.default = mockimocki;
mockimocki.supply = supply;

module.exports = mockimocki;
