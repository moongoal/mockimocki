/* eslint global-require: "off", no-unused-expressions: "off" */

const { expect } = require('chai');
const { mockimocki, supply } = require('.');

describe('mockimocki', () => {
  const newMock = opts => mockimocki(
    () => 'real',
    () => 'mock',
    opts
  );

  beforeEach(() => {
    supply();
  });

  describe('module', () => {
    it('should work as a function', () => {
      const m = require('.');

      expect(m(
        () => 'x',
        () => 'y'
      )).to.equal('x');
    });

    it('should export at least the two main functions', () => {
      const m = require('.');

      expect(m.mockimocki).to.exist;
      expect(m.supply).to.exist;
    });

    it('should export the `mockimocki()` function as ES6 default module export', () => {
      const m = require('.');

      expect(m).to.equal(m.default);
    });
  });

  describe('mockimocki()', () => {
    it('should return the global type of data by default', () => {
      expect(newMock()).to.equal('real');
    });

    it('should enforce the `force` option', () => {
      expect(newMock({ force: 'real' })).to.equal('real');
      expect(newMock({ force: 'mock' })).to.equal('mock');
    });

    it('should throw an exception when a bad value is passed as `force`', () => {
      expect(() => { newMock({ force: 'rael' }); }).to.throw();
    });
  });

  describe('supply()', () => {
    it('should make the module return mock/real data as asked', () => {
      supply('mock');
      expect(newMock()).to.equal('mock');

      supply('real');
      expect(newMock()).to.equal('real');
    });

    it('should throw an exception when a bad value is passed as `what`', () => {
      expect(() => { supply('moc'); }).to.throw();
    });
  });
});
