# Mock data switch

Mockimocki works as a simple switch between real data and mock data. You provide both as functions and choose - either globally or locally - what to supply, mockimocki gives it back.

## Import

Either

```javascript
  import { mockimocki, supply } from 'mockimocki';
  // Or
  import mockimocki from 'mockimocki';

  // Then
  mockimocki(...);
  mockimocki.mockimocki(...); // Same as `mockimocki(...)`
  mockimocki.supply(...);

```

Or

```javascript
  const { mockimocki, supply } = require('mockimocki');
  // Or
  const mockimocki = require('mockimocki');

  // Then
  mockimocki(...);
  mockimocki.mockimocki(...); // Same as `mockimocki(...)`
  mockimocki.supply(...);
```

## Usage

```javascript
  function getMyRecord (id) {
    return mockimocki(
      () => getRealRecord(id),
      () => getMockRecord(id)
    );
  }
```

To **globally** set what kind of data you want to supply when calling the mockimocki function, use the `supply()` function:

```javascript
  mockimocki.supply('real');
  // Or
  mockimocki.supply('mock');
```

The `mockimocki()` function accepts a third argument, an object of options, where you can specify what data to use, **locally** overriding the choice you made:

```javascript
function getMyRecord (id) {
  return mockimocki(
    () => getRealRecord(id),
    () => getMockRecord(id),
    { force: 'mock' }
  );
}
```

This can come in handy during debugging, where you only want (not) to mock a specific piece of data.

## API
### mockimocki(real, mock, [opts])
Return either the result of `real()` or `mock()`.

#### Arguments
- real: A function returning the real data
- mock: A function returning the mock data
- opts: An object of options

#### Options
Currently only the `force` option is provided, which can assume the same value as the `what` parameter of `supply()`. `force` is used to locally override the type of data you want the `mockimocki()` function to return. Every time some data is forced, a warning is emitted on the console.

### supply(what)
Choose what data to supply globally.

#### Arguments
- what: either 'real' or 'mock'

## License
This module and its contents are published under the BSD 3 Clause license (BSD-3-Clause).
See the included *LICENSE* file.
